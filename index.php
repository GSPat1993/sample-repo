<?php
  require 'vendor/autoload.php';

  date_default_timezone_set('Asia/Singapore');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Origin: *');

  header('Access-Control-Allow-Methods: GET, POST');

  header("Access-Control-Allow-Headers: X-Requested-With");

  $mandrill = new Mandrill('a507bd7f3459fa620884236fead1f42d-us14');
  $result = $mandrill->users->info();
  print_r($result);
?>